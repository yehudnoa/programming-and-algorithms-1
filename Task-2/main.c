#include <stdio.h>

int is_palindrome(int n, char binary[])
{
    for(int i=0; i<n; i++, n--)
    {
        if(binary[n] != binary[i])
            return 0;
    }
    return 1;
}

int main() {
    long long int low, high, change;
    int n = 0, count = 0, error=0, r, remainder= 0;
    char binary[64] = {0};
    char z, letters[36]={'0', '1', '2', '3', '4', '5', '6', '7', '8',
                         '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
                         'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                         'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    printf("Intervals:\n");
    while (1) {
        error = scanf("%s %d %lld %lld", &z, &r, &low, &high);
        if(error==EOF)
            break;
        if ( error != 4 || high < 0 || low < 0 || (z != 'c' && z != 'l') || r >36 || r<2 ||
            low > high) {
            printf("Invalid input.\n");
            return 2;
        }
        while (high >= low) {
            change = low;
                n = 0;
                if(change % r !=0 || change==0){
                    while (change != 0) {
                        remainder = change % r;
                        remainder=letters[remainder];
                        binary[n] = remainder;
                        change /= r;
                        n += 1;
                    }
                    n -= 1;
                    if (is_palindrome(n, binary)) {
                        if (z == 'l') {
                            printf("%lld = ", low);
                            if (n == -1) {
                                printf("0");
                            }
                            for (int i = 0; i <= n; i++) {
                                printf("%c", binary[i]);
                            }
                            printf(" (%d)\n", r);
                        }
                        count++;
                    }
                }
                low++;
        }
        if (z == 'c') {
            printf("Total: %d\n", count);
        }
        //error = 0; z = 0; high = 2; low =1;
        count = 0;
    }
    return 0;
}