The task is to develop a program to search for numbers that are symmetric when expressed in a given radix. The problem is a generalization of the simpler problem, there is an additional parameter in the input - the radix. It is recommended to start with the simpler problem. Once the simpler program works, extend it accept the radix parameter.

The input of the program is a sequence of search commands. The sequence is read and the commands are executed until the program reaches the end of input (EOF). Each search command consists of four fields: X R LO HI. The first character denotes the command to execute:

c - count the symmetric numbers in the interval,
l - list the symmetric numbers in the interval.
Following the command, there is an integer R that denotes the radix and then two integers that denote the lower and upper bounds of the interval to process. The given interval is considered a closed interval, i.e., both LO and HI are tested by the command.
The output of the program are the answers to the input commands. The answer to the l command is the list of symmetric numbers, the exact format is shown below. The answer to the c command is the total number of matching numbers.

The program must validate input data. If the input is invalid, the program must detect it, it shall output an error message (see below), and terminate. If displayed, the error message must be sent to the standard output (do not send it to the error output) and the error message must be terminated by a newline (\n). The input is considered invalid, if:

the command is not known (i.e., neither l nor c),
the radix is not a number, or does not fit the range <2;36>,
interval boundaries are not valid integers, or are missing,
the lower bound is negative,
the lower bound is greater than the upper bound.

Sample program runs:
Intervals:
l 2 0 20
0 = 0 (2)
1 = 1 (2)
3 = 11 (2)
5 = 101 (2)
7 = 111 (2)
9 = 1001 (2)
15 = 1111 (2)
17 = 10001 (2)
c 2 0 20
Total: 8
l 3 0 40
0 = 0 (3)
1 = 1 (3)
2 = 2 (3)
4 = 11 (3)
8 = 22 (3)
10 = 101 (3)
13 = 111 (3)
16 = 121 (3)
20 = 202 (3)
23 = 212 (3)
26 = 222 (3)
28 = 1001 (3)
40 = 1111 (3)
l 5 0 50
0 = 0 (5)
1 = 1 (5)
2 = 2 (5)
3 = 3 (5)
4 = 4 (5)
6 = 11 (5)
12 = 22 (5)
18 = 33 (5)
24 = 44 (5)
26 = 101 (5)
31 = 111 (5)
36 = 121 (5)
41 = 131 (5)
46 = 141 (5)
l 17 1918 2019
1927 = 6b6 (17)
1944 = 6c6 (17)
1961 = 6d6 (17)
1978 = 6e6 (17)
1995 = 6f6 (17)
2012 = 6g6 (17)
c 31 38 12345
Total: 396

Intervals:
c 6 1234567 7654321
Total: 4955
l 16 255 255
255 = ff (16)
x 18 25 97
Invalid input.

Intervals:
c 100 90 100
Invalid input.

Intervals:
l radix 10 20
Invalid input.