#include <stdio.h>
#include <math.h>
#include <string.h>

void sort(double temp, double *a, double *b, double *c)
{

    if (*c > *a){
        temp = *c;
        *c = *a;
        *a = temp;}
    if(*b > *a){
        temp = *b;
        *b = *a;
        *a = temp;}
    if(*c > *b){
        temp = *c;
        *c = *b;
        *b = temp;}
}

int main() {
    char str[4], str1[] = "SSS", str2[] = "SAS", str3[] = "ASA";
    double a, a1, b, c, x=0, y=0, z=0, temp;
    double e = 1e-12;
    int n, m = 0;

    while (m < 2) {
        if (m == 0) {
            printf("Triangle #1:\n");
            if (scanf("%3s %lf %lf %lf", str, &a, &b, &c) != 4 || a <= 0 || b <= 0 || c <= 0) {
                printf("Invalid input.\n");
                return 0;
            }
        }

        if (m == 1) {
            printf("Triangle #2:\n");
            if (scanf("%3s %lf %lf %lf", str, &a, &b, &c) != 4 || a <= 0 || b <= 0 || c <= 0) {
                printf("Invalid input.\n");
                return 0;
            }
        }

        if (strcmp(str, str1) == 0)
            n = 1;
        else if (strcmp(str, str2) == 0)
            n = 2;
        else if (strcmp(str, str3) == 0)
            n = 3;
        else {
            printf("Invalid input.\n");
            return 0;
        }

        switch (n) {
            case 1:
                sort( temp, &a, &b, &c);

                break;

            case 2:
                if (b >= 180 || b == 0) {
                    printf("Invalid input.\n");
                    return 0;
                }

                a1 = pow(a, 2) + pow(c, 2) - (2 * a * c) * cos(b * M_PI / 180);
                b = a;
                a = sqrt(a1);

                sort( temp, &a, &b, &c);

                break;

            case 3:
                if (a >= 180 || c >= 180 || a == 0 || c == 0) {
                    printf("Invalid input.\n");
                    return 0;
                }

                if ((a + c) >= 180) {
                    printf("The input does not form a triangle.\n");
                    return 0;
                }

                temp = 180 - a - c;
                temp = temp * M_PI / 180;
                a = (b / (sin(temp))) * (sin(a * M_PI / 180));
                c = (b / (sin(temp))) * (sin(c * M_PI / 180));

                sort( temp, &a, &b, &c);
                break;
        }

        n = 0;

        if ((a + b - c) > e * fmax(fabs(a), fabs(b)) && (b + c - a) > e * fmax(fabs(b), fabs(c)) && (a + c - b) > e * fmax(fabs(a), fabs(c))) {
            if (m == 0) {
                x = a;
                y = b;
                z = c;
                a = 0;
                b = 0;
                c = 0;
            }
            m++;
        }
        else
        {
            printf("The input does not form a triangle.\n");
            return 0;
        }
    }

    if (fabs(a - x) < e * fmax(fabs(x), fabs(a)) && fabs(b - y) < e * fmax(fabs(b), fabs(y)) &&
        fabs(c - z) < e * fmax(fabs(c), fabs(z)))    //checks if the triangles are the same
    {
        printf("The triangles are identical.\n");
        return 0;
    } else if (fabs((a / x) - (b / y)) < e * fmax(fabs(a / x), fabs(b / y)) && fabs((b / y) - (c / z)) < e * fmax(fabs(b / y), fabs(c / z)) &&
               fabs((a / x) - (c / z)) < e * fmax(fabs(a / x), fabs(c / z)))    //check if triangles are similar
    {
        printf("The triangles are not identical, however, they are similar.\n");
        return 0;
    } else                //if they are neather
    {
        printf("The triangles are neither identical nor similar.\n");
        return 0;
    }
}
