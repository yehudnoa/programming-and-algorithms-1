The task is to develop a program that compares two given triangles. This problem is an extension of the simpler variant. The triangles in this assignment may be entered in one of the following three variants: SSS, SAS, ASA (see below). We recommend to first solve the basic problem. Next, use the solution from the simple problem and extend it to accept the inputs in this problem.

There are given two triangles. Each triangle is defined in one of the three following variants:

SSS - the triangle is defined by the lengths of its sides,
SAS - the triangle is defined by the lengths of two sides and the included angle (the angle is in degrees, the values are entered as side angle side),
ASA - the triangle is defined by a side and two adjacent angles (the angles are in degrees, the values are entered as angle side angle).
Your program reads the numbers from the standard input. Next, the program decides one of the following cases:
the input does not form a triangle,
the two triangles are identical,
the two triangles are similar, but not identical, or
the two triangles are entirely different.
The program must validate input data. If the input is invalid, the program must detect it, it shall output an error message (see below), and terminate. If displayed, the error message must be sent to the standard output (do not send it to the error output) and the error message must be terminated by a newline (\n). The input is considered invalid, if:

the input is not in any known form (i.e., is not any of SSS, SAS, ASA),
the sides are invalid (are not valid decimal numbers),
side lengths are invalid (negative or zero),
the angles are invalid (below or equal to 0 degrees, or above or equal to 180 degrees),
some side(s) are missing.


Sample program runs:

Triangle #1:
SSS 4 6.5 7
Triangle #2:
SSS 7 6.5 4
The triangles are identical.

Triangle #1:
SSS 7 7 7
Triangle #2:
ASA 60 13 60
The triangles are not identical, however, they are similar.

Triangle #1:
  SSS
   4.5
 6       7
Triangle #2:
SAS 7 30 12
The triangles are neither identical nor similar.

Triangle #1:
SSS 9.861 9.865 9.883
Triangle #2:
SSS 9861 9883 9865
The triangles are not identical, however, they are similar.

Triangle #1:
ASA 60 11 60
Triangle #2:
SAS 13 60 13
The triangles are not identical, however, they are similar.

Triangle #1:
SAS 10 180 20
Invalid input.
Triangle #1:
ASA 120 20 90
The input does not form a triangle.

Triangle #1:
S SS 20 30 30
Invalid input.
Triangle #1:
SSS 20 30 50
The input does not form a triangle.

Triangle #1:
ASA -4 10 12
Invalid input.

Triangle #1:
SAS 1 2 abcd
Invalid input.