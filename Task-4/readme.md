The task is to develop a program to help airplane navigation and avoid collisions.

Airport control tower needs to monitor airplanes and prevent possible collisions. This is backed up by a radar and a control software. Your task is to develop the software which receives radar data and detects possible airplane collisions, i.e., it finds the minimum distance among the flights reported by the radar.

The input of your program is a list of flights. We assume a simplified problem where each flight is represented by a 2-D coordinate - x and y. Both x and y are decimal numbers. Following the coordinate, there is a name of the flight, the name is terminated by a newline. This way, each input line describes one flight. The number of flights may be very high and the total number of flights is not known beforehand. Moreover, flight names may be surprisingly long. The input format is shown in the sample runs below. Input processing ends when EOF is reached.

The output of the program is the minimum distance found among all pairs of input flights. There may exist one or even more flight pairs whose distance is equal to the minimum distance found. Therefore, the program outputs all such pairs.

The program must scrutinize input data. If an invalid input is detected, the program must display an error message and terminate. The following is considered invalid input:

non-numeric coordinate,
extra or missing brace/comma,
the total number of flights is less than 2 (at least two flights are required to compute some distance).
Your program will be tested in a restricted environment. The testing environment limits running time and available memory. The exact time and memory limits are shown in the reference solution testing log. The program may require a lot of time to compute the result if the number of flights is high. A reasonable implementation of the naive algorithm passes all mandatory test. However, it will not pass the bonus test. An improved algorithm is required to pass the bonus test. Next, the input may very long. There is not an explicit upper limit on the number of flights and there is not any explicit upper limit on the length of flight names (may vary from few characters to thousands characters). The program needs to use dynamic memory allocation to store the input data and fit into the memory limits.

Sample program runs:
Enter flights:
[0,0] KLM KL1981
[5, 0] Etihad Airways ETD26
[10, 0] British Airways BA123
[7, 0] Emirates UAE93P
[ 2 , 0 ] Wizz Air W67868
Minimum distance: 2.000000
KLM KL1981 - Wizz Air W67868
Etihad Airways ETD26 - Emirates UAE93P

Enter flights:
[0,5] Lufthansa LH948
[5,0] British Airways BA164
[0,0] Rynair FR2861
[5,5] Air France AF1886
[2.5,2.5] Korean Air KAL902
Minimum distance: 3.535534
Lufthansa LH948 - Korean Air KAL902
Rynair FR2861 - Korean Air KAL902
Korean Air KAL902 - British Airways BA164
Korean Air KAL902 - Air France AF1886

Enter flights:
[-10,-5] Air Malta AMC103
[10,0] easyJet EZY8732
[12,12] SAS SAS534
Minimum distance: 12.165525
easyJet EZY8732 - SAS SAS534
Enter flights:
[-1000000,0] Pegasus Airlines PGT1163
[1000000,0] Austrian Airlines AUA849
[5000000,0] Air India AIC142
Minimum distance: 2000000.000000
Pegasus Airlines PGT1163 - Austrian Airlines AUA849

Enter flights:
[10,10] Air Progtest
[10,10] PA1 Airways
[20, 20] PA2 Airways
[20,20] Segmentation Faultways
[20,20] PS1 sHellways
[10,10] AAG Wings
Minimum distance: 0.000000
Air Progtest - PA1 Airways
Air Progtest - AAG Wings
PA1 Airways - AAG Wings
PA2 Airways - Segmentation Faultways
PA2 Airways - PS1 sHellways
Segmentation Faultways - PS1 sHellways

Enter flights:
[3,abc] Air Progtest
Invalid input.

Enter flights:
[0,0] Air Progtest
[5,8 PA1 Airways
[10,10] Segmentation Faultways
Invalid input.