#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct flight_info{    /** my info for each flight */
    double x;
    double y;
    char *str;
}flight;

void free_str(flight info[], int size){
    for(; size>=0; size--){
        free(info[size].str);
    }
    free(info);
}

void distence(flight info[], int i, double *min){
    double new_min, x, y;

    for(int c = i-1; c >= 0; c--){            /** gets distence from new input to all the other inputs */
        x = info[i].x - info[c].x;
        y = info[i].y - info[c].y;
        x = pow(x,2);
        y = pow(y,2);
        new_min = sqrt(x+y);
        if(new_min<*min || *min == -2){     /** if smaller min then old new min then we have a new min */
            *min = new_min;
        }                                   /** originaly here i had anothere 2-D array that i put the numbers
                                              * of the arrays in structs that were min ditence , like 1, 2 - means
                                              * first struct and second struct so later i can just print out the
                                              * output, but every time i got a new min a had to clear the array and doing
                                              * memory allocation to that kept getting me a memory allocation so i took
                                              * it out */
    }
}

char* scan(){
    char *k=(char*)malloc(sizeof(flight));
    int q;
    for ( q = 0; (( k[q] = getchar()) !='\n' ); ++q ){
        if(k[q]=='\t' && k[q-1] != ' '){
            k[q]=' ';
        }else if(k[q]=='\t' && k[q-1] == ' ')  {
            q--;
        }else if(k[q]==' ' && k[q-1] == ' ')   {
            q--;
        }else
        k=(char*)realloc(k, (q+2)*(sizeof(char)));
    }
    k[q]='\0';
    return k;
}

int main() {
    int i=0, n=0, e=2;
    int p;
    char c;

    double min = -2;
    flight *info;
    info=(flight*)malloc(sizeof(flight));         /** malloc for the flight struct */


    printf("Enter flights:\n");
    while ((p=scanf(" [ %lf , %lf %1c ", &info[i].x, &info[i].y,&c)) && p==3) {   /** here i take in the info till EOF */
        (info+(i))->str=scan();
        distence(info, i, &min);      /** go to distence function */
        i++;
        e++;
        info=(flight*)realloc(info,e * sizeof(flight));     /** here i realloc the flight info struct every time one is added */

        if (c!=']'){                                                /** for some reason they want that if the ] isnt there print
                                                                      * error only after all the inputs are in, dont know why, the
                                                                      * people who write these hw's are wierd*/
            n=1;
        }
    }
    i--;
    if(n==0 && min != -2 && p==EOF) {                                        /** printing out flights that are the min distence apart, and the min distence */
        printf("Minimum distance: %lf\n", min);
        double new_min, x, y;
        double e = 0.1e-5;
        for(int g=i; g>0; g--) {
            for (int r = g - 1; r >= 0; r--) {                      /** this is redundent but i sisnt know how to fix my memory problems */
                x = info[g].x - info[r].x;
                y = info[g].y - info[r].y;
                x = pow(x, 2);
                y = pow(y, 2);
                new_min = sqrt(x + y);
                if (fabs(new_min - min) <= e * fmax(new_min, min)) {
                    printf("%s - %s\n", info[g].str, info[r].str);
                }
            }
        }


        free_str(info, i);

    }else{
        printf("Invalid input.\n");     /** if invalid cause only one input or ] wasnt there  */
        free_str(info, i);
        return 1;
    }
}
