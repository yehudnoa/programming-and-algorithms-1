The task is to develop a program to search phrases offered in autocomplete.

Our program shall simulate autocomplete functionality. The program holds a list of well-known phrases. Each phrase is ranked, the rank is a number describing the frequency of the phrase. Our program shall read the well-known phrases and search them based on a string. It shall find all phrases where the given string is a substring.

The input of the program is a list of well-known phrases. The input has the form:

number:phrase
where number is a decimal number describing the rank (frequency) and phrase is an arbitrary ASCII string. The total number of well-known phrases is unknown, the listing ends with an empty line. Following the well-known phrases, there is a list of strings to autocomplete. Each input line is one string to autocomplete, i.e. a string to search in the list of previously read well-known phrases. The processing of input phrases stops when EOF is reached.
The output of the program is the total number of phrases that match to the given search string. The search string may be located anywhere in the phrase (the phrase does not have to start with the search string, this is the difference from the basic problem), moreover, the searching shall be case insensitive. Following the total number of matching phrases, there shall be the list of the matching phrases. The phrases shall be listed in an order of descending rank (frequency). The program shall list at most 50 of the most relevant phrases. Such answer shall be displayed for each input search string.

The program must validate input data. If the input is invalid, the program must detect it, it shall output an error message (see below) and terminate. If displayed, the error message must be displayed on the standard output (do not send it to the error output) and the error message must be terminated by a newline (\n). The input is considered invalid, if:

the rank (frequency) is missing for an input phrase,
the rank (frequency) is not a decimal number,
missing the colon separating the rank and the phrase itself,
there were 0 phrases in the input.
Your program will be tested in a restricted environment. The testing environment limits running time and available memory. The exact time and memory limits are shown in the reference solution testing log. There are small inputs used in the mandatory tests, thus a reasonable implementation of the naive algorithm shall pass the mandatory tests. There is a bonus test included, the bonus test test the program with long inputs (many phrases, long phrases, many search strings). An improved algorithm and careful coding is required to pass the bonus test.

Sample program runs:
Frequent searches:
80:Progtest random test failed
70:Segmentation fault
40:Progtest homework #2
80:Invalid input data
50.5:Program has stopped working
15:Validator result
20:Lid is open
60:Test in progress

Searches:
test
Found: 3
> Progtest random test failed
> Test in progress
> Progtest homework #2
test fail
Found: 1
> Progtest random test failed
lid
Found: 3
> Invalid input data
> Lid is open
> Validator result
prog
Found: 4
> Progtest random test failed
> Test in progress
> Program has stopped working
> Progtest homework #2
data
Found: 1
> Invalid input data
ISO
Found: 0
exception
Found: 0

Frequent searches:
80 foo
Invalid input.

Frequent searches:
Progtest. Progtest never changes.
Invalid input.