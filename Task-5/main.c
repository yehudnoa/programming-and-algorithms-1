#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct data {
    double index_number;
    char *s;
    char *buffer;
} indx;

void free_indx(indx *i, int size, int sizeb) {
    for (int a = 0; a <= size+1; a++) {
        free(i[a].buffer);
    }
    for (; sizeb >= 0; sizeb--)
        free(i[sizeb].s);

    free(i);
}

int dComp(const void *x, const void *y) {
    const indx *dx = (const indx *) x;
    const indx *dy = (const indx *) y;
    if (dx->index_number > dy->index_number) return -1;
    if (dx->index_number < dy->index_number) return 1;
    else return 0;
}

void removeLF(char *str) {
    unsigned len = strlen(str);
    if (len > 0 && str[len - 1] == '\n') {
        str[len - 1] = '\0';
    }
}

void rec_compair(indx *heystack, char *needle, int count, int found) {
    if (count == -1 || found==50)
        printf("Found: %d\n", found);
    else {
        if (strcasestr(heystack[count].s, needle) !=NULL) {
            rec_compair(heystack, needle, --count, ++found);
            printf("> %s\n", heystack[count+1].s);
        } else {
            rec_compair(heystack, needle, --count, found);
        }
    }
}

int main() {
    size_t sizet;
    int sum, count = 0, bytes_read;
    char *refrence;
    indx *i = NULL;

    printf("Frequent searches:\n");

    while (1) {
        i = (indx *) realloc(i, (count+1) * sizeof(indx));
        i[count].buffer = NULL;
        bytes_read = getline(&i[count].buffer, &sizet, stdin);

        if (bytes_read == 1) {
            break;
        }

        i[count].s = (char *) malloc(strlen(i[count].buffer) * (sizeof(char)));
        sum = sscanf(i[count].buffer, "%lf:%[^\n]s", &i[count].index_number, i[count].s);

        if (sum != 2) {
            break;
        }

        count++;
    }
    count--;
    if (bytes_read == 1 && count > 1) {

        qsort(i, count + 1, sizeof(i[0]), dComp);


        printf("Searches:\n");

        while (1) {
            refrence = NULL;
            bytes_read = getline(&refrence, &sizet, stdin);
            if (bytes_read != -1) {
                removeLF(refrence);
                rec_compair(i, refrence, count, 0);
                free(refrence);
            } else {
                free(refrence);
                free_indx(i, count, count);
                return 0;
            }
            
        }
    } else {
        printf("Invalid input.\n");
        free_indx(i, count, count+1);
        return 1;
    }
    
}
