#include <stdio.h>

typedef struct wt{          //* this is water tank struct, each one represents a different water tank, inside the stuct are its important characteristics
    int alt;                //* the altitude
    int sa;                 //* this is the surface area (width * length)
    int abhight;            //* the absolute height of the water tank (altitude + height)
}wt;


int up(wt tank[], int size, int v, double *th) {                                        //* this function we get the surface
                                                                                        //* here we slowly take the last used hight and the next closest hight that something changes with the surface area
                                                                                        //* and we calculate the volume in that section and then we check if the added volume is making the total calculated
                                                                                        //* volume more/ equal less then the given volume, if it is less we continue the looping, if equal or more we exit

    int count = 0, tsa, nh = 0, nv = 0, min = tank[0].alt, msa = 0, last = tank[0].alt; //* *count* keeps track of the wt were on; *tsa* is the top surface area; *nh* is the new hight we are at
                                                                                        //* *nv* is the new volume; *msa* is the the surface area were subtracting from the total surface area;
    *th = tank[count].alt;                                                              //* *th* is the total hight including the starting alt, its starting from the lowest alt
    tsa = tank[count].sa;                                                               //* the tsa started with the surface area of the lowest alt

    while (nv < v) {                                                                    //* this loop sees when we reach a volume that is either equal or a bit more then the volume we need
        tsa -= msa;                                                                     //* if the hight is above the hight of a water tank we remove its surface from the tsa
        msa = 0;                                                                        //* reset the msa
        while (min == tank[count + 1].alt && count < size) {                            //* seeing if any of the next tanks have the same sa as the min hight were at, if yes is gets added to tsa
            tsa += tank[count + 1].sa;
            count++;
        }
        if ((count + 1) < size)                                                         //* finding the next hight when the surface area changes
            min = tank[count + 1].alt;
        else {
            for (int i = count; i >= 0; i--) {
                if (tank[i].abhight > min)
                    min = tank[i].abhight;
            }
        }
        for (int i = count; i >= 0; i--) {                                              //* getting the volume of the new section
            if (tank[i].abhight < min && tank[i].abhight > last)
                min = tank[i].abhight;
        }
        last = min;
        for (int i = count; i >= 0; i--) {                                             //* finding the wt's that there hight is equal to the top of the section and will not be included in the next loop
            if (tank[i].abhight == min)
                msa += tank[i].sa;
        }
        nh = min - *th;     //* (new hight = hight of the section)
        nv += (tsa * nh);   //* ( volume of the section is added into new volume )
        *th += nh;          //* ( the hight of the section is added into the total hight )
    }
    double h;                                                                          //* h is if the volume we calculated is more the the given volume, h is what we are subtracting so we get the correct hight
    nv -= v;                                                                           //* the diffrence between the calulated volume and the given volume
    h = (double) nv / tsa;
    *th -= h;                                                                          //* decreasing the extra hight of the extra volume from the total hight
    return 0;
}

void sortalt(wt tank[], int n) {                                                       //* sort structs from lowest alt to highest
    int i, j, imin;

    for (i=0; i<n-1; i++) {
        imin = i;
        for (j=i+1; j<n; j++)
            if (tank[j].alt < tank[imin].alt)
                imin = j;
        if (imin!=i) {
            wt tmp = tank[imin];
            tank[imin] = tank[i];
            tank[i] = tmp;
        }
    }
}

void vol_cal(int volume, int size, int tot_vol, wt tanks[]){                //* in this funtion we see if the volume fits in the water tanks
    double surface;

    if (volume == 0) {                                                      //* if empty
        printf("Empty.\n");
    }
    else {
        for (int i = size - 1; i >= 0; i--) {                               //* if volume given bigger then volume in all water tanks together
            tot_vol += ((tanks[i].abhight-tanks[i].alt) * tanks[i].sa);
        }
        if (tot_vol < volume) {
            printf("Overflow.\n");
        }

        else {
            sortalt(tanks, size);                                           //* sort the stucts by altitudes
            up(tanks, size, volume, &surface);                              //* if all is good with the volume go to up function
            printf("h = %lf\n", surface);
        }
    }
}

int main()
{
    int size, hight, width, depth, volume, tot_vol=0, error=0;

    printf("Enter number of reservoirs:\n");

    if(scanf("%d", &size) !=1 || size > 200000 || size < 1)         //* this part gives us the size and checks that the input is correct
    {
        printf("Invalid input.\n");
        return 1;   //* ****ERROR****
    }

    wt tanks[200000];

    printf("Enter reservoir parameters:\n");
    for(int i=0; i<size; i++)                                               //* here we get the inputs for each reservoir
    {
         if( scanf("%d %d %d %d", &tanks[i].alt, &hight, &width, &depth) !=4 || hight < 1 || width < 1 || depth < 1){
             printf("Invalid input.\n");
             return 2; //* ****ERROR****
         }
         tanks[i].sa= width * depth;
         tanks[i].abhight = hight + tanks[i].alt;
    }

    printf("Enter volume:\n");
    while (1) {
        error = scanf("%d", &volume);                               //* this is when the actual volume is collected
        if(error==EOF)                                                      //* checking is user inputted EOF
            break;
        if( error != 1 || volume < 0) {                                     //* now checking the input is correct
            printf("Invalid input.\n");
            return 3;   //* ****ERROR****
        }
        vol_cal( volume, size, tot_vol, tanks);                             //* go to the function
        volume=0;
    }
    return 0;   //* ****THE END****
}