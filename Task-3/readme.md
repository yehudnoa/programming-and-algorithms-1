The task is to develop a program which computes the utilization of a water reservoir system.

We assume a water reservoir system. The system consists of water reservoirs and a pipes. Each reservoir is of a prismatic shape. The reservoirs are all connected by water pipes. There may be many reservoirs installed, the total number of reservoirs does not exceed 200000. For the sake of simplicity, we assume that the volume of water in pipes is zero (all water is in the reservoirs). The reservoir system may look like:


The program is given in the description of the reservoirs. Then there follow certain queries. Each query is an integer number - the total water volume in the reservoirs. The program must compute water level for each volume queried.

The input of your program is the number of reservoirs N (an integer). Then there is description of the individual reservoirs. Each reservoir is described by a 4-tuple of integers Alt H W D where Alt is the altitude of the bottom, H is the height, W is the width and D is the depth of the reservoir (all dimensions in meters). Following the reservoirs, there is a sequence of queries. Each query is an integer number - the volume of water that is to be stored. The volume is given in cubic meters. The queries are finished when EOF (end-of-file) is detected in the standard input.

The output of the program is the altitude of water level in the reservoirs if the input volume of water is stored. There are two borderline cases: if the water volume is zero, then the program must answer "Empty". Second, if the water volume exceeds the cumulative capacity, the answer is "Overflow". The exact formats are shown in the sample runs below.

The program must scrutinize input data. If invalid input is detected, the program must display an error message and terminate. The following is considered an invalid input:

the number of reservoirs N is non-numeric, negative, zero, or exceeds 200000,
the altitude Alt is non-numeric,
the dimensions H, W, or D are non-numeric, negative, or zero,
the volume queried is non-numeric or negative.
Your program will be tested in a restricted environment. The testing environment limits running time and available memory. The exact time and memory limits are shown in the reference solution testing log. The program is not memory greedy. However, the program may require a lot of time to compute the result if N is high. A correct implementation of the straightforward algorithm passes all tests except the bonus (thus it will be awarded 100%). To pass the bonus tests, an improved algorithm must be used. There is high number of reservoirs N and high number of queries in the bonus tests.

Sample program runs:
Enter number of reservoirs:
5
Enter reservoir parameters:
0 3 2 2
0 5 1 2
0 2 3 3
4 10 1 1
20 5 1 1
Enter volume:
0
Empty.
3
h = 0.200000
10
h = 0.666667
32
h = 2.333333
50
h = 14.000000
1000
Overflow.
53
h = 23.000000

Enter number of reservoirs:
3
Enter reservoir parameters:
-2 10 1 1
0 6 3 3
2 2 5 5
Enter volume:
1
h = -1.000000
2
h = 0.000000
3
h = 0.100000
22
h = 2.000000
23
h = 2.028571
95
h = 4.300000
112
h = 6.000000
113
h = 7.000000
114
h = 8.000000
115
Overflow.

Enter number of reservoirs:
5
Enter reservoir parameters:
0 5 1 1
0 5 2 2
2 3 3 3
2 5 4 4
2 5 5 5
Enter volume:
100
h = 3.636364
50
h = 2.727273
-30
Invalid input.

Enter number of reservoirs:
5
Enter reservoir parameters:
0 5 1 abcd
Invalid input.
