#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define LIST_BY_YEAR       0
#define LIST_BY_TYPE       1

#define TYPE_MAX           100
#define SETUP_MAX          100

typedef struct TEngine
{
    struct TEngine * m_Next;
    struct TEngine * m_Prev;
    int              m_Year;
    char             m_Type  [ TYPE_MAX ];
    int              m_Setup [ SETUP_MAX ];
} TENGINE;

typedef struct TArchive
{
    struct TArchive * m_Next;
    struct TArchive * m_Prev;
    TENGINE         * m_Engines;
} TARCHIVE;

TENGINE * createEngine ( const char * type, int year )
{
    TENGINE * res = (TENGINE *) malloc ( sizeof  (*res ) );
    res -> m_Next = NULL;
    res -> m_Prev = NULL;
    res -> m_Year = year;
    strncpy ( res -> m_Type, type, sizeof ( res -> m_Type ) );
    for ( int i = 0; i < SETUP_MAX; i ++ )
        res -> m_Setup[i] = 0;
    return res;
}
#endif /* __PROGTEST__ */

TARCHIVE * createTemp(){
    TARCHIVE * temp = (TARCHIVE *) malloc (sizeof (*temp ));
    temp->m_Prev = NULL;
    temp->m_Next = NULL;
    return temp;
}

void PushList(TARCHIVE * first, TARCHIVE * second, TARCHIVE * third ){
    if(first != NULL){
        first->m_Next = second;
        second->m_Prev = first;
    }
    if (third != NULL){
        second->m_Next = third;
        third->m_Prev = second;
    }
}

void AddSame(TENGINE * first, TENGINE * second){
    if( first->m_Next != NULL ){
        second->m_Next = first->m_Next;
        first->m_Next->m_Prev = second;
    }
    if( second->m_Prev != NULL ){
        second->m_Prev->m_Next = first;
        first->m_Prev = second->m_Prev;
    }
    first->m_Next = second;
    second->m_Prev = first;
}

TARCHIVE * beginingP( TARCHIVE * list){
    while (list->m_Prev != NULL){
        list = list->m_Prev;
    }
    return list;
}

TARCHIVE * EmptyList(TENGINE * engine){
    TARCHIVE * list;
    list = (TARCHIVE *) malloc (sizeof (*list ));
    list->m_Next = NULL;
    list->m_Prev = NULL;
    list->m_Engines = engine;
    return list;
}

TARCHIVE * AddPartMiddle(TARCHIVE * Point, TENGINE * engine){
    TARCHIVE * temp;
    temp = createTemp();
    temp->m_Engines = engine;
    PushList(Point->m_Prev, temp, Point);
    Point = beginingP(Point);
    return Point;
}

TARCHIVE * AddPartEnd(TARCHIVE * Point, TENGINE * engine){
    TARCHIVE * temp;
    temp = createTemp();
    temp->m_Engines = engine;
    PushList(Point, temp, Point->m_Next);
    Point = beginingP(Point);
    return Point;
}

TENGINE * cleanEng(TENGINE * engine){
    engine->m_Next = NULL;
    engine->m_Prev = NULL;
    return engine;
}

TARCHIVE * AddEngine ( TARCHIVE * list, int listBy, TENGINE * engine ) {
    int i;
    TARCHIVE *temp2 = NULL;

    if(engine == NULL){
        return list;
    }

    if(engine->m_Prev != NULL || engine->m_Next != NULL){
        engine = cleanEng(engine);
    }

    if (list == NULL) {
        list = EmptyList(engine);
        return list;
    } else if (listBy == LIST_BY_YEAR) {
        temp2 = list;
        while (1) {
            // if less add befor temp
            if (engine->m_Year < temp2->m_Engines->m_Year) {
                list = AddPartMiddle(temp2, engine);
                return list;
                // if same sort by year, add threw engine
            } else if (engine->m_Year == temp2->m_Engines->m_Year) {
                i = (strcmp(engine->m_Type, temp2->m_Engines->m_Type));
                while (1) {
                    if (i <= 0) {
                        AddSame(engine, temp2->m_Engines);
                        while (temp2->m_Engines->m_Prev != NULL) {
                            temp2->m_Engines = temp2->m_Engines->m_Prev;
                        }
                        return list;
                    } else if (i > 0) {
                        if (temp2->m_Engines->m_Next != NULL) {
                            temp2->m_Engines = temp2->m_Engines->m_Next;
                        } else {
                            AddSame(temp2->m_Engines, engine);
                            while (temp2->m_Engines->m_Prev != NULL) {
                                temp2->m_Engines = temp2->m_Engines->m_Prev;
                            }
                            return list;
                        }
                    }
                }
            }else{
                // if bigger and next is not NULL then push temp to next and restart
                if( temp2->m_Next != NULL )
                    temp2 = temp2->m_Next;
                // if temp is not NULL add too last
                else{
                    list = AddPartEnd(temp2, engine);
                    return list;
                }
            }
        }
    }else if (listBy == LIST_BY_TYPE){
        temp2 = list;
        while(1) {
            i=(strcmp(engine->m_Type, temp2->m_Engines->m_Type));
            if (i < 0) {
                list = AddPartMiddle(temp2, engine);
                return list;
            } else if (i == 0) {
                while (1) {
                    if (engine->m_Year <= temp2->m_Engines->m_Year) {
                        AddSame(engine, temp2->m_Engines);
                        while (temp2->m_Engines->m_Prev != NULL) {
                            temp2->m_Engines = temp2->m_Engines->m_Prev;
                        }
                        return list;
                    } else if (engine->m_Year > temp2->m_Engines->m_Year) {
                        if (temp2->m_Engines->m_Next != NULL) {
                            temp2->m_Engines = temp2->m_Engines->m_Next;
                        } else {
                            AddSame(temp2->m_Engines, engine);
                            while (temp2->m_Engines->m_Prev != NULL) {
                                temp2->m_Engines = temp2->m_Engines->m_Prev;
                            }
                            return list;
                        }
                    }
                }
            } else {
                if (temp2->m_Next != NULL)
                    temp2 = temp2->m_Next;
                else {
                    list = AddPartEnd(temp2, engine);
                    return list;
                }
            }
        }
    }
    return list;
}

void DelArchive ( TARCHIVE * list ){
    TENGINE * EngTemp;

    if(list != NULL){
        while ( list ) {
            while(list->m_Engines->m_Next){
                list->m_Engines = list->m_Engines->m_Next;
            }
            while(list->m_Engines->m_Prev){
                EngTemp = list->m_Engines;
                list->m_Engines = list->m_Engines->m_Prev;
                free( EngTemp );
            }
            while(list->m_Engines){
                EngTemp = list->m_Engines;
                list->m_Engines = list->m_Engines->m_Prev;
                free( EngTemp );
            }
            TARCHIVE * p = list -> m_Next;
            free ( list );
            list = p;
        }
    }
}

TARCHIVE * ReorderArchive ( TARCHIVE * list, int listBy )
{
    TARCHIVE * new_list = NULL;
    TENGINE * engine =NULL, * engine1 = NULL;

    if(list == NULL){
        return list;
    }

    engine = list->m_Engines->m_Next;
    new_list = AddEngine( new_list, listBy, list->m_Engines);
    while(1){
        while(engine != NULL){
            engine1 = engine->m_Next;
            new_list = AddEngine( new_list, listBy, engine);
            engine = engine1;
        }
        if( list->m_Next != NULL ) {
            list = list->m_Next;
            engine1 = list->m_Engines;
            engine = engine1->m_Next;
            new_list = AddEngine(new_list, listBy, engine1);
            free(list->m_Prev);
        }else{
            break;
        }
    }
    free(list);
    return new_list;
}


#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
{
    TARCHIVE * a;

    a = NULL;
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "TDI 1.9", 2010 ) );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next == NULL
             && a -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "TDI 1.9", 2005 ) );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next == NULL
             && a -> m_Next
             && a -> m_Next -> m_Prev == a
             && a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Engines -> m_Next == NULL
             && a -> m_Next -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "TSI 1.2", 2010 ) );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next == NULL
             && a -> m_Next
             && a -> m_Next -> m_Prev == a
             && a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TSI 1.2" )
             && a -> m_Next -> m_Engines -> m_Next -> m_Prev == a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Next -> m_Next == NULL
             && a -> m_Next -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "TDI 2.0", 2005 ) );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 2.0" )
             && a -> m_Engines -> m_Next -> m_Prev == a -> m_Engines
             && a -> m_Engines -> m_Next -> m_Next == NULL
             && a -> m_Next
             && a -> m_Next -> m_Prev == a
             && a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TSI 1.2" )
             && a -> m_Next -> m_Engines -> m_Next -> m_Prev == a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Next -> m_Next == NULL
             && a -> m_Next -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "MPI 1.4", 2005 ) );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Type, "MPI 1.4" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Next -> m_Prev == a -> m_Engines
             && a -> m_Engines -> m_Next -> m_Next -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Next -> m_Next -> m_Type, "TDI 2.0" )
             && a -> m_Engines -> m_Next -> m_Next -> m_Prev == a -> m_Engines -> m_Next
             && a -> m_Engines -> m_Next -> m_Next -> m_Next == NULL
             && a -> m_Next
             && a -> m_Next -> m_Prev == a
             && a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TSI 1.2" )
             && a -> m_Next -> m_Engines -> m_Next -> m_Prev == a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Next -> m_Next == NULL
             && a -> m_Next -> m_Next == NULL );
    a = ReorderArchive ( a, LIST_BY_TYPE );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Type, "MPI 1.4" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next == NULL
             && a -> m_Next
             && a -> m_Next -> m_Prev == a
             && a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Next -> m_Prev == a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Next -> m_Next == NULL
             && a -> m_Next -> m_Next
             && a -> m_Next -> m_Next -> m_Prev == a -> m_Next
             && a -> m_Next -> m_Next -> m_Engines
             && a -> m_Next -> m_Next -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Next -> m_Next -> m_Engines -> m_Type, "TDI 2.0" )
             && a -> m_Next -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
             && a -> m_Next -> m_Next -> m_Next
             && a -> m_Next -> m_Next -> m_Next -> m_Prev == a -> m_Next -> m_Next
             && a -> m_Next -> m_Next -> m_Next -> m_Engines
             && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Type, "TSI 1.2" )
             && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
             && a -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    DelArchive ( a );

    a = NULL;
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "TDI 1.9", 2010 ) );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next == NULL
             && a -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "TDI 1.9", 2005 ) );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Next -> m_Prev == a -> m_Engines
             && a -> m_Engines -> m_Next -> m_Next == NULL
             && a -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "TSI 1.2", 2010 ) );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Next -> m_Prev == a -> m_Engines
             && a -> m_Engines -> m_Next -> m_Next == NULL
             && a -> m_Next
             && a -> m_Next -> m_Prev == a
             && a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TSI 1.2" )
             && a -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Engines -> m_Next == NULL
             && a -> m_Next -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "TDI 2.0", 2005 ) );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Next -> m_Prev == a -> m_Engines
             && a -> m_Engines -> m_Next -> m_Next == NULL
             && a -> m_Next
             && a -> m_Next -> m_Prev == a
             && a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 2.0" )
             && a -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Engines -> m_Next == NULL
             && a -> m_Next -> m_Next
             && a -> m_Next -> m_Next -> m_Prev == a -> m_Next
             && a -> m_Next -> m_Next -> m_Engines
             && a -> m_Next -> m_Next -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Next -> m_Engines -> m_Type, "TSI 1.2" )
             && a -> m_Next -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
             && a -> m_Next -> m_Next -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "MPI 1.4", 2005 ) );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Type, "MPI 1.4" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next == NULL
             && a -> m_Next
             && a -> m_Next -> m_Prev == a
             && a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Next -> m_Prev == a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Next -> m_Next == NULL
             && a -> m_Next -> m_Next
             && a -> m_Next -> m_Next -> m_Prev == a -> m_Next
             && a -> m_Next -> m_Next -> m_Engines
             && a -> m_Next -> m_Next -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Next -> m_Next -> m_Engines -> m_Type, "TDI 2.0" )
             && a -> m_Next -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
             && a -> m_Next -> m_Next -> m_Next
             && a -> m_Next -> m_Next -> m_Next -> m_Prev == a -> m_Next -> m_Next
             && a -> m_Next -> m_Next -> m_Next -> m_Engines
             && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Type, "TSI 1.2" )
             && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
             && a -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "TDI 1.9", 2010 ) );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Type, "MPI 1.4" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next == NULL
             && a -> m_Next
             && a -> m_Next -> m_Prev == a
             && a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Next -> m_Prev == a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Prev == a -> m_Next -> m_Engines -> m_Next
             && a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Next == NULL
             && a -> m_Next -> m_Next
             && a -> m_Next -> m_Next -> m_Prev == a -> m_Next
             && a -> m_Next -> m_Next -> m_Engines
             && a -> m_Next -> m_Next -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Next -> m_Next -> m_Engines -> m_Type, "TDI 2.0" )
             && a -> m_Next -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
             && a -> m_Next -> m_Next -> m_Next
             && a -> m_Next -> m_Next -> m_Next -> m_Prev == a -> m_Next -> m_Next
             && a -> m_Next -> m_Next -> m_Next -> m_Engines
             && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Type, "TSI 1.2" )
             && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
             && a -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    a = ReorderArchive ( a, LIST_BY_YEAR );
    assert ( a
             && a -> m_Prev == NULL
             && a -> m_Engines
             && a -> m_Engines -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Type, "MPI 1.4" )
             && a -> m_Engines -> m_Prev == NULL
             && a -> m_Engines -> m_Next -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
             && a -> m_Engines -> m_Next -> m_Prev == a -> m_Engines
             && a -> m_Engines -> m_Next -> m_Next -> m_Year == 2005
             && ! strcmp ( a -> m_Engines -> m_Next -> m_Next -> m_Type, "TDI 2.0" )
             && a -> m_Engines -> m_Next -> m_Next -> m_Prev == a -> m_Engines -> m_Next
             && a -> m_Engines -> m_Next -> m_Next -> m_Next == NULL
             && a -> m_Next
             && a -> m_Next -> m_Prev == a
             && a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Prev == NULL
             && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
             && a -> m_Next -> m_Engines -> m_Next -> m_Prev == a -> m_Next -> m_Engines
             && a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Year == 2010
             && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Type, "TSI 1.2" )
             && a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Prev == a -> m_Next -> m_Engines -> m_Next
             && a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Next == NULL
             && a -> m_Next -> m_Next == NULL );
    DelArchive ( a );

    return 0;
}
#endif /* __PROGTEST__ */