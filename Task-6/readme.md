The task is to implement functions (not a whole program, just functions) which manipulate an archive. The archive holds structures that describe the settings needed to control combustion engines of certain type.

The settings of a combustion engine control unit is an important evidence in the "dieselgate" cause. The actual settings are different for different engines and different production dates. The setting itself is just a set of numbers. The values are, however, secret. Indeed, your task is to implement functions that organize an archive of such settings, however, your program will not actually manipulate the values in the settings itself.

The archive is organized as doubly linked lists - see the figures. Structure TARCHIVEforms a linked list, field m_Next points to the next element in the list, field m_Prev to the previous element. The branches (m_Engines) point to another linked lists, the list hold structures that describe the individual engines. The ordering of the structures is not random, indeed, the archive is organized in either one of the two following ways:

LIST_BY_YEAR - the branches m_Engines reference linked lists that describe engines with a common production year. The list of TARCHIVE is sorted in an ascending order by production year. Each TENGINE linked list is sorted in an ascending order by the type string.

LIST_BY_TYPE - the branches m_Engines reference linked lists that describe engines with the same type string. The list of TARCHIVE is sorted in an ascending order by type string. Each TENGINE linked list is sorted in an ascending order by the production year.

The required interface is:

#define LIST_BY_YEAR       0
#define LIST_BY_TYPE       1

#define TYPE_MAX           100
#define SETUP_MAX          100

typedef struct TEngine
 {
   struct TEngine * m_Next;
   struct TEngine * m_Prev;
   int              m_Year;
   char             m_Type  [ TYPE_MAX ]; 
   int              m_Setup [ SETUP_MAX ];
 } TENGINE;

typedef struct TArchive
 {
   struct TArchive * m_Next;   
   struct TArchive * m_Prev;   
   TENGINE         * m_Engines;
 } TARCHIVE;

TARCHIVE  * AddEngine      ( TARCHIVE        * list,    
                             int               listBy,  
                             TENGINE         * engine );
void        DelArchive     ( TARCHIVE        * list );
TARCHIVE * ReorderArchive  ( TARCHIVE        * list,
                             int               listBy );
TENGINE
is a structure to describe an engine. Field m_Year denotes production year, field m_Type is an ASCIIZ string containing engine type name. Field m_Next is a link to the next engine in the list (or is NULL for the last engine in the list). Field m_Prev is a link to the previous engine in the list, or NULL for the first engine in the list. Array m_Setup is filled with the settings of the engine. Your implementation does not actually need to access this field. Since the setup is secret, your are not even allowed to read or copy it. Thus, your implementation shall always use the existing TENGINE structures and shall not create new.
TARCHIVE
is an auxiliary structure to connect TENGINE lists. Field m_Next points to the next element in the TARCHIVE list (NULL for the last element), field m_Prev points to the previous element in the TARCHIVE list (NULL for the first element), field m_Engines is a pointer to the TENGINE list.
AddEngine(list, listBy, engine)
parameter list is a pointer to the existing archive. The function adds new engine settings to the archive. Parameter engine is a pointer pointing to a dynamically structure allocated, the structure has filled all fields. Parameter sortBy describes the organization of the archive, its value is either of LIST_BY_YEAR / LIST_BY_TYPE. Your function shall include the new engine into the existing archive, i.e. it shall change the pointers inside the archive and/or engine. Do not copy the structure, use the existing structure passed by the parameter. The function shall return a pointer to the updated archive.
DelArchive(list)
the function frees dynamically allocated memory used to represent the archive list.
ReorderArchive(list, listBy)
the function re-arranges the engines in the existing archive list to match the ordering described by parameter listBy (LIST_BY_YEAR / LIST_BY_TYPE). The function must preserve the existing TENGINE structures (update the links only). On the other hand the function may create/delete TARCHIVE structures. Return value is a pointer to the archive after the modifications.
Submit a source file with the implementation of the required functions AddEngine, DelArchive and ReorderArchive. Further, the source file must include your auxiliary functions which are called from within the required functions. The functions will be called from the testing environment, thus, it is important to adhere to the required interface. Use the sample code in the attached archive as a basis for your development. There is an example main with some test in the sample code. These values will be used in the basic test. Please note the header files as well as main is nested in a conditional compile block (#ifdef/#endif). Please keep these conditional compile block in place. They are present to simplify the development. When compiling on your computer, the headers and main will be present as usual. On the other hand, the header and main will "disappear" when compiled by Progtest. Thus, your testing main will not interfere with the testing environment's main.

Your function will be executed in a limited environment. There are limits on both time and memory. The exact limits are shown in the test log of the reference. The goal of this homework is to train linked lists. It is expected that the functions implement naive algorithms. Indeed, there is not much room for optimization.

